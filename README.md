# BookStore

Develop a RESTful API for a bookstore that allows a user to login, perform user related tasks, view a list of
books and place book orders

## Design

### Class

![](./out/docs/uml/uml_class/class_01.png)

### Sequence Diagram

#### POST: /login

  User login authentication API.

> in
  + body
    + username
    + password 

> out
  + service status (status, code, message, ...)
  + token_id (if success)

> validate
  + required
    + username + password 

![](./out/docs/uml/uml_seq/login.png)

---

#### GET: /users
 Gets information about the logged in user. A successfully authenticated request returns
information related to the user and the books ordered.

(Login required) 

> in
  + header (required)
    + token_id
    + username
  
> out
  + service status (status, code, message, ...)
  + user (user info)
  + listOrder[
      orderInfo,
      bookList[]
    ]

> validate
  + header (required)


![](./out/docs/uml/uml_seq_user_order/get_user_order.png)

---

#### DELETE: /users
  Delete logged in user’s record and order history
  ( delete from user_main but copy to user_main_del for keeping log and user_id relase to order for tracking back in feature)

(Login required) 
> in
  + header (required)
    + token_id
    + username
  
> out
  + service status (status, code, message, ...)

> validate
  + header (required)


![](./out/docs/uml/uml_seq_user_del/del_user.png)

---

#### POST: /users
  Create a user account and store user’s information in Users table (DB).

(Login not required) 
> in
  + body (required)
    + username
    + password
    + date_of_birth (option)
  
> out
  + service status (status, code, message, ...)
  + user (user info)
  + listOrder[
      orderInfo,
      bookList[]
    ]

> validate
  + date_of_birth is dd/mm/yyyy
  + username, password minimum is 5 character

![](./out/docs/uml/uml_seq_user_create/create_user.png)

---

#### POST: /users/orders
Order books and store order information in Orders table (DB). This returns the price for a
successful order

(Login required) 
> in
   + header (required)
    + token_id
    + username
  + body (required)
    + itemList[]
      + id
      + price
    + shippingAddress
  
> out
  + service status (status, code, message, ...)
  + price

> validate
  + header (required)
  + shippingAddress not be empty
  + itemList[{id}] is valid (found)

![](./out/docs/uml/uml_seq_order/create_order.png)

---

#### GET: /books

Gets a list of books from an external book publisher’s web services and returns the
list sorted alphabetically with the recommended books always appears first. The should be no
duplicated books in the list.

(Login not required) 
> in
  + header (optional)
    + token_id
    + username
  
> out
  + bookList[]

>  validate
  + none

>  Other
  + caching -> spring-boot-starter-cache and caffeine
  
![](./out/docs/uml/uml_seq_catalog/get_books.png)

---

## Tools/Framework (build.gradle)
+ use springframework.boot version 2.1.8.RELEASE and gradle-6.7 
+ MYSQL: MYSQL is a open-source relational database management system.
+ Lombok: Project Lombok is a java library that automatically plugs into editor and build tools, spicing up java. Getter, Setters, Construcutors can be created with annotation without writing the code with the help of Lombok
+ Swagger: Swagger is open-source framework that helps developers design, build, document and consume RESTful Web services.
+ Logstash: logging - Logstash is part of the Elastic Stack along with Beats, Elasticsearch and Kibana. Logstash is a server-side data processing pipeline
+ Caffeine: provides an in-memory cache using a Google Guava inspired API. The improvements draw on our experience designing Guava's cache and ConcurrentLinkedHashMap.
  
---

## Steps To Run Application

### Swagger ui
  + http://localhost:8080/swagger-ui.html

### DB

#### new install
+ Install MySQL in your laptop if you do not have one. You can download it from here:
 [ https://www.mysql.com/downloads/](https://dev.mysql.com/downloads/workbench/)


#### use online sql server (free 5MB)

+ use mysql online server (free) https://www.freesqldatabase.com/account/


```
Host: sql6.freesqldatabase.com
Database name: sql6443368
Database user: sql6443368
Database password: <contact for password from synicxt@gmail.com>
Port number: 3306

```
  + login to manage db at https://www.phpmyadmin.co/db_structure.php?db=sql6443368


---

### table script
+ create table from this script
  
> user_main
> + user main for login

``` 
CREATE TABLE IF NOT EXISTS `sql6443368`.`user_main` ( `user_id` VARCHAR(50) NOT NULL , `loginname` VARCHAR(50) NOT NULL , `password` VARCHAR(255) NOT NULL , `name` VARCHAR(100) NOT NULL , `surname` VARCHAR(100) NOT NULL , `date_of_birth` DATE NULL , `create_date` TIMESTAMP NOT NULL , `last_login_date` DATETIME NULL , `token_id` VARCHAR(50) NULL , PRIMARY KEY (`user_id`(50)), INDEX `idx_token_id` (`token_id`(50)), UNIQUE `uniq_login_name` (`loginname`(50))) ENGINE = InnoDB;

```
> user_main_del
> + hold data of deleted user

``` 
CREATE TABLE IF NOT EXISTS `sql6443368`.`user_main_del` ( `user_id` VARCHAR(50) NOT NULL , `loginname` VARCHAR(50) NOT NULL , `password` VARCHAR(255) NOT NULL , `name` VARCHAR(100) NOT NULL , `surname` VARCHAR(100) NOT NULL , `date_of_birth` DATE NULL , `create_date` TIMESTAMP NOT NULL , `last_login_date` DATETIME NULL , `delete_date` TIMESTAMP NULL , 
INDEX `user_id` (`user_id`(50)), INDEX `idx_login_name_del` (`loginname`(50))) ENGINE = InnoDB;

```

> order_main
> + main order info

``` 
CREATE TABLE IF NOT EXISTS `sql6443368`.`order_main` ( `user_id` VARCHAR(50) NOT NULL , `order_id` VARCHAR(50) NOT NULL COMMENT 'order_id' , `order_date` TIMESTAMP NOT NULL , `total_price` DOUBLE NOT NULL, `shipping_address` VARCHAR(1000) NOT NULL COMMENT 'address to ship' , INDEX `idx_order_user_id` (`user_id`(50)), INDEX `idx_order_date` (`order_date`), UNIQUE `uniq_order_id` (`order_id`(50))) ENGINE = InnoDB;

``` 

> order_item
> + item relate to order_id from order_main

```
CREATE TABLE IF NOT EXISTS `sql6443368`.`order_item` ( `order_id` VARCHAR(50) NOT NULL , `item_id` INT NOT NULL , `item_name` VARCHAR(255) NOT NULL , `item_author` VARCHAR(255) NOT NULL , `item_price` DOUBLE NOT NULL , `recommened` BOOLEAN NOT NULL , `item_quantity` INT NOT NULL , INDEX `idx_order_id` (`order_id`(50)), INDEX `idx_item_id` (`item_id`)) ENGINE = InnoDB;

```

> order_status
> + hold status of order from OPEN -> PAID -> SHIPPING -> COMPLETE (or CANCEL)
> + status_date_to is null mean last status (orther is previous)
> + not implement now :P

```
CREATE TABLE `sql6443368`.`order_status` ( `order_id` VARCHAR(50) NOT NULL , `order_status` VARCHAR(30) NOT NULL DEFAULT 'OPEN' COMMENT 'OPEN|PAID|SHIPING|COMPLETE|CANCEL' , `status_date_from` TIMESTAMP NOT NULL , `status_date_from_by` VARCHAR(50) NOT NULL COMMENT 'by user_id' , `status_date_to` DATETIME NULL COMMENT 'if null is not close status' , `status_date_to_by` VARCHAR(50) NULL , INDEX `idx_sta_order_id` (`order_id`(50)), INDEX `idx_order_status` (`order_status`(30)), INDEX `idx_status_from` (`status_date_from`), INDEX `idx_status_to` (`status_date_to`)) ENGINE = InnoDB;

```

### set datasource configuration (to your db)
  + /BookStore/src/main/resources/application.properties

```
bookstore.datasource.jdbc-url=jdbc:mysql://sql6.freesqldatabase.com:3306/sql6443368
bookstore.datasource.username=sql6443368
bookstore.datasource.password=<contact for password from synicxt@gmail.com>

```



## TEST

### test script (curl)
  + see in folder : ./test_result/curl/curl_script.txt
  
*** beware must login for get token_id for use most of service required login

### test output (json)
  + see in folder : ./test_result/output

