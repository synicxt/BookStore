package com.test.app;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class DataSourceConfiguration {

	@Primary
	@Bean(name = "bookstoreDataSource")
	@ConfigurationProperties(prefix="bookstore.datasource")
	public DataSource getBookstoreDataSource() {			
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "bookstoreJdbcTemplate")
	@Autowired(required = true)
	public NamedParameterJdbcTemplate getBookstoreJdbcTemplate(@Qualifier("bookstoreDataSource") DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
}
