package com.test.app.api;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.dao.BookStoreDao;
import com.test.app.api.exception.UserSessionException;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DemoService {
	@Value("${spring.application.name:default_value}")
	private String applicatoinName;
	
	@Autowired
	private BookStoreDao bookDao;
	
	@PostMapping("/demo/test")
	public HashMap<String, Object> getNotification(
			@RequestBody String req
	) throws Throwable {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		
		log.info(" req = "+req);
		
		String sysdate = bookDao.test();
		
		resp.put("IN", req);
		resp.put("sysdate", sysdate);
		
		return resp;
	}
	
	@PostMapping("/demo/no_authorize")
	public HashMap<String, Object> noAuthorize(
			@RequestBody String req
	) throws Throwable {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		
		if(1==1) throw new UserSessionException("not found token_id");
		
		
		return resp;
		
	}
}
