package com.test.app.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.dao.OrderDao;
import com.test.app.api.dao.UserDao;
import com.test.app.api.entity.BookInfo;
import com.test.app.api.entity.OrderCreateInfo;
import com.test.app.api.entity.OrderItem;
import com.test.app.api.entity.StaticMap;
import com.test.app.api.entity.UserInfo;
import com.test.app.api.exception.ServiceParameterValidationException;
import com.test.app.api.service.BookService;
import com.test.app.api.utils.ErrorUtil;
import com.test.app.api.utils.ServiceParameterValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class OrderService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	BookService bService;
	
	@PostMapping("/users/order")
	public HashMap<String, Object> createOrder(
			@RequestHeader(value = "token_id", required = true) String tokenId,
			@RequestHeader(value = "username", required = true) String userName,
			@RequestBody OrderCreateInfo req
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		
		String prefix = userName+"|createOrder|";

		log.info(prefix);
		try {
			
			//-- validate size
			if(req == null || req.getItemList() == null || req.getItemList().size() == 0) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_VALIDATE);
				res.put("message", ApplicationConstant._FAIL_CODE_VALIDATE_MSG);
				res.put("messageTech", "invalid parameter array size 0");
				
				return res;
			}
			
			//-- validate shippingAddress
			ServiceParameterValidator.getInstance()
			.setSubject("shippingAddress", req.getShippingAddress())
			.required(ApplicationConstant._VALIDATE_NOT_EMPTY)
			.validate();
			
			//-- validate book id
			if(StaticMap.getAllMap().isEmpty()) {
				List<BookInfo> rList = bService.getRecomendBooks(userName);
				List<BookInfo> bList = bService.getAllBooks(userName);
				
				HashMap<Integer, BookInfo> allMap = new HashMap<Integer, BookInfo>();
				for(BookInfo x: bList) {	// all
					allMap.put(x.getId(), x);
				}
				
				for(BookInfo x: rList) {	// recommend
					x.setRecommended(true);
					allMap.put(x.getId(), x);
				}
				
				StaticMap.setAllMap(allMap);
			}
			
			List<Integer> wrongList = new ArrayList<Integer>();
			double sumPrice = 0.00d;
			
			for(OrderItem r: req.getItemList()) {	
				if(StaticMap.getAllMap().get(r.getId()) == null) {
					wrongList.add(r.getId());
				}
				else
				{
					sumPrice += r.getQuantity() * StaticMap.getAllMap().get(r.getId()).getPrice();
				}
				
			}
			
			if(wrongList.size() > 0) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_VALIDATE_ID);
				res.put("message", ApplicationConstant._FAIL_CODE_VALIDATE_ID_MSG.replaceAll("\\$\\{param}", ""+wrongList));
				res.put("messageTech", "invalid parameter miss match");
				
				return res;
			}
			
					
			UserInfo user = userDao.validateUserToken(userName, tokenId);
			
			if(user == null) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_TOKEN);
				res.put("message", ApplicationConstant._FAIL_CODE_TOKEN_MSG);
				res.put("messageTech", "invalid token_id");
				
				return res;
			}
			
			//-- here code for order
			UUID uuid = UUID.randomUUID();
			String orderId = uuid.toString().replace( "-", "" ).toLowerCase();
			
			orderDao.saveOrder(userName, user.getUserId(), orderId, req, sumPrice);
			
			res.put("price", sumPrice);
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(ServiceParameterValidationException e) {
			throw e;
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			if(ErrorUtil.getNestException(e).toLowerCase().indexOf("duplicate entry") >= 0) {
				res.put("code", ApplicationConstant._FAIL_CODE_LOGIN_DUP);
				res.put("message", ApplicationConstant._FAIL_CODE_LOGIN_DUP_MSG);
			}
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
}
