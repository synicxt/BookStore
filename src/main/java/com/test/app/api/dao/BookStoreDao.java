package com.test.app.api.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class BookStoreDao {

	@Qualifier("bookstoreJdbcTemplate")
	@Autowired(required = true)
	private NamedParameterJdbcTemplate db;
	
	public String test() throws Throwable {
		// TODO Auto-generated method stub
		String prefix = this.getClass().getSimpleName()+"|test|";
		log.info(prefix);

		String script = "select sysdate() as cur_date";
		
		Map<String, Object> param = new HashMap<>();
		
		
		log.info(prefix+"param = "+param);
		
		String sysdate = "";
		
		try  {
			
			SqlRowSet rs =  db.queryForRowSet(script, param);
			
			if (rs.next()) {
				sysdate = rs.getString("cur_date");
				log.info(prefix+" append "+sysdate);
				
			}

		} catch ( Throwable t) {
			throw t;
		}finally {
		
		}
		
		return sysdate;
	}
}
