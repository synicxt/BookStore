package com.test.app.api.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.test.app.api.entity.LoginRequest;
import com.test.app.api.entity.UserCreateInfo;
import com.test.app.api.entity.UserInfo;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDao {

	@Qualifier("bookstoreJdbcTemplate")
	@Autowired(required = true)
	private NamedParameterJdbcTemplate db;
	
	public String login(LoginRequest req) throws Throwable {
		// TODO Auto-generated method stub
		String prefix = req == null ? "N/A": req.getUsername()+"|"+this.getClass().getSimpleName()+"|login|";
		log.info(prefix+"req = "+req);

		String script = "select user_id from user_main where loginname=:loginname and password=MD5(:password) ";
		
		Map<String, Object> param = new HashMap<>();
		param.put("loginname", req.getUsername());
		param.put("password", req.getPassword());
		
		// log.info(prefix+"param = "+param);
		
		String user_id = null;
		
		try  {
			
			SqlRowSet rs =  db.queryForRowSet(script, param);
			
			if (rs.next()) {
				user_id = rs.getString("user_id");
			}

		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" user_id="+user_id);
		}
		
		return user_id;
	}

	public void updateToken(String username, String token_id) throws Throwable {
		// TODO Auto-generated method stub
		String prefix = username+"|"+this.getClass().getSimpleName()+"|updateToken|";
		log.info(prefix+"username = "+username);

		String script = "update user_main set token_id=:token_id, last_login_date=NOW() where loginname=:loginname ";
		
		Map<String, Object> param = new HashMap<>();
		param.put("loginname", username);
		param.put("token_id", token_id);
		
		// log.info(prefix+"param = "+param);
		int effect = 0;
		
		try  {
			
			effect = db.update(script, param);
			
		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" effect="+effect);
		}
		
	}

	public void addUser(UserCreateInfo req, String user_id) throws SQLException, Throwable {
		// TODO Auto-generated method stub
		String prefix = req.getUsername()+"|"+this.getClass().getSimpleName()+"|addUser|";
		log.info(prefix);

		String script = "INSERT INTO user_main (user_id, loginname, password, name, surname, date_of_birth, create_date) "
				+" VALUES (:user_id, :loginname, MD5(:password), :name, :surname, STR_TO_DATE(:date_of_birth, '%d/%m/%Y'), CURRENT_TIMESTAMP) ";
		
		Map<String, Object> param = new HashMap<>();
		param.put("loginname", req.getUsername());
		param.put("user_id", user_id);
		param.put("password", req.getPassword());
		param.put("name", req.getName());
		param.put("surname", req.getSurname());
		param.put("date_of_birth", req.getDateOfBirth());
		
		// log.info(prefix+"param = "+param);
		int effect = 0;
		
		try  {
			effect = db.update(script, param);
		}
		catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" effect="+effect);
		}
		
	}

	public UserInfo validateUserToken(String userName, String tokenId) throws SQLException, Throwable {
		String prefix = userName+"|validateUserToken|";
		log.info(prefix+"tokenId = "+tokenId);

		String script = "select user_id, loginname, name, surname, DATE_FORMAT(date_of_birth, '%d/%m/%Y') as date_of_birth from user_main where loginname=:loginname and token_id=:token_id ";
		
		Map<String, Object> param = new HashMap<>();
		param.put("loginname", userName);
		param.put("token_id", tokenId);
		
		// log.info(prefix+"param = "+param);
		
		UserInfo user = null;
		
		try  {
			
			SqlRowSet rs =  db.queryForRowSet(script, param);
			
			if (rs.next()) {
				user = new UserInfo();
				user.setDateOfBirth(rs.getString("date_of_birth"));
				user.setName(rs.getString("name"));
				user.setSurname(rs.getString("surname"));
				user.setUserId(rs.getString("user_id"));
			}

		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" UserInfo="+user);
		}
		
		return user;
	}

	public void deleteUser(String userName, String userId) throws SQLException, Throwable {
		// TODO Auto-generated method stub
		
		String prefix = userName+"|deleteUser|";

		String scriptCopy = "INSERT INTO user_main_del (user_id, loginname, password, name, surname, date_of_birth, create_date, last_login_date) "
				+" (select user_id, loginname, password, name, surname, date_of_birth, create_date, last_login_date from user_main where user_id=:user_id) ";
		
		String scriptDel = "delete from user_main where user_id=:user_id";
		
		Map<String, Object> param = new HashMap<>();
		param.put("user_id", userId);
		
		log.info(prefix+"param = "+param);
		
		int rec = 0;
		try  {
			
			rec = db.update(scriptCopy, param);
			
			if(rec > 0)
				db.update(scriptDel, param);

		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" effect="+rec);
		}
		
	}
}
