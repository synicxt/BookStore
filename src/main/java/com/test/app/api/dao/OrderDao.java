package com.test.app.api.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.test.app.api.entity.BookInfo;
import com.test.app.api.entity.BookInfoOrder;
import com.test.app.api.entity.OrderCreateInfo;
import com.test.app.api.entity.OrderInfo;
import com.test.app.api.entity.OrderItem;
import com.test.app.api.entity.StaticMap;
import com.test.app.api.entity.UserInfo;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class OrderDao {

	@Qualifier("bookstoreJdbcTemplate")
	@Autowired(required = true)
	private NamedParameterJdbcTemplate db;
	
	public List<OrderInfo> getListOrder(UserInfo user, String userName) throws SQLException, Throwable {
		String prefix = userName+"|getListOrder|";
		log.info(prefix);

		String script = "select o.order_id, DATE_FORMAT(o.order_date, '%d/%m/%Y %H:%i:%S')  as order_date, o.shipping_address, o.total_price, "
				+" t.item_id, t.item_name, t.item_author, t.item_price, t.recommened, t.item_quantity "
				+" from order_main o LEFT JOIN order_item t on t.order_id=o.order_id "
				+" where o.user_id=:user_id "
				+" order by o.order_date desc, o.order_id desc, t.item_name";
		
		Map<String, Object> param = new HashMap<>();
		param.put("user_id", user.getUserId());
		
		log.info(prefix+"param = "+param);
		
		List<OrderInfo> list = new ArrayList<OrderInfo>();
		List<BookInfoOrder> bookList = new ArrayList<BookInfoOrder>();
		
		String tmpOrderId = "";
		
		try  {
			
			SqlRowSet rs =  db.queryForRowSet(script, param);
			
			while (rs.next()) {
				if(!tmpOrderId.equals(rs.getString("order_id"))) {
					tmpOrderId = rs.getString("order_id");
					
					OrderInfo o = new OrderInfo();
					o.setOderDate(rs.getString("order_date"));
					o.setOrderId(rs.getString("order_id"));
					o.setShippingAddress(rs.getString("shipping_address"));
					o.setTotalPrice(rs.getDouble("total_price"));
					list.add(o);
					
					bookList = new ArrayList<BookInfoOrder>();
					o.setBookList(bookList);
					
					log.info(prefix+"append order="+o);
				}
				
				BookInfoOrder b = new BookInfoOrder();
				b.setAuthor(rs.getString("item_author"));
				b.setId(rs.getInt("item_id"));
				b.setName(rs.getString("item_name"));
				b.setPrice(rs.getDouble("item_price"));
				b.setQuantity(rs.getInt("item_quantity"));
				b.setRecommended(rs.getBoolean("recommened"));
				bookList.add(b);
				
				log.info(prefix+"append book="+b);
			}

		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" list size="+list.size());
		}
		
		return list;
	}

	public void saveOrder(String userName, String userId, String orderId, OrderCreateInfo req, double sumPrice) throws SQLException, Throwable {
		// TODO Auto-generated method stub
		String prefix = userName+"|saveOrder|";
		log.info(prefix+"orderId="+orderId+", sumPrice = "+sumPrice+", req="+req);

		String scriptMain = "INSERT INTO order_main (user_id, order_id, shipping_address, total_price) "
				+" VALUES (:user_id, :order_id, :shipping_address, :total_price)";
		
		String scriptItem = "INSERT INTO order_item (order_id, item_id, item_name, item_author, item_price, recommened, item_quantity) "
				+" VALUES (:order_id, :item_id, :item_name, :item_author, :item_price, :recommened, :item_quantity) ";
		
		Map<String, Object> param = new HashMap<>();
		param.put("order_id", orderId);
		param.put("user_id", userId);
		param.put("shipping_address", req.getShippingAddress());
		param.put("total_price", sumPrice);
		
		log.info(prefix+"param = "+param);
		
		int rec = 0;
		try  {
			
			rec = db.update(scriptMain, param);
			
			if(rec > 0) {
				List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
				HashMap<Integer, BookInfo> m = StaticMap.getAllMap();
				
				for(OrderItem item: req.getItemList()) {
					param = new HashMap<>();
					param.put("order_id", orderId);
					param.put("item_id", item.getId());
					param.put("item_quantity", item.getQuantity());
					
					if(m.get(item.getId()) != null) {
						param.put("item_name", m.get(item.getId()).getName());
						param.put("item_author", m.get(item.getId()).getAuthor());
						param.put("item_price", m.get(item.getId()).getPrice());
						param.put("recommened", m.get(item.getId()).isRecommended());
						
						tmp.add(param);
					}					
				}
				
				log.info(prefix+"batchUpdate for item");
				Map<String, Object>[] batchValues = new HashMap[tmp.size()];
				tmp.toArray(batchValues);
				db.batchUpdate(scriptItem, batchValues);
			}

		} catch ( Throwable t) {
			throw t;
		}finally {
			log.info(prefix+" effect="+rec);
		}
	}

}
