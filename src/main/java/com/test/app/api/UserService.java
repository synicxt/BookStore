package com.test.app.api;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.dao.OrderDao;
import com.test.app.api.dao.UserDao;
import com.test.app.api.entity.OrderInfo;
import com.test.app.api.entity.UserCreateInfo;
import com.test.app.api.entity.UserInfo;
import com.test.app.api.exception.ServiceException;
import com.test.app.api.utils.ErrorUtil;
import com.test.app.api.utils.ServiceParameterValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserService {

	@Autowired
	UserDao userDao;
	
	@Autowired
	OrderDao orderDao;
	
	@PostMapping("/users")
	public HashMap<String, Object> createUser(
			@RequestBody UserCreateInfo req
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		String visitorId = (StringUtils.isEmpty(req.getUsername()) ? "N/A" : req.getUsername());
		String prefix = visitorId+"|createUser|";
		
		ServiceParameterValidator.getInstance()
		.setSubject("username", req.getUsername())
		.matched(ApplicationConstant._FAIL_CODE_STR_LEN_MSG, ApplicationConstant._REGX_STRING_LEN_5)
		
		.setSubject("password", req.getPassword())
		.matched(ApplicationConstant._FAIL_CODE_STR_LEN_MSG, ApplicationConstant._REGX_STRING_LEN_5)
		
		.setSubject("name", req.getName())
		.required(ApplicationConstant._VALIDATE_NOT_EMPTY)
		
		.setSubject("surname", req.getSurname())
		.required(ApplicationConstant._VALIDATE_NOT_EMPTY)
		.validate();
		
		if(!StringUtils.isEmpty(req.getDateOfBirth()))
			ServiceParameterValidator.getInstance()
			.setSubject("date_of_birth", req.getDateOfBirth())
			.matched(ApplicationConstant._VALIDATE_NOT_DATE, ApplicationConstant._REGX_DATE_FORMAT)
			.validate();
		
		log.info(prefix);
		try {
					
			UUID uuid = UUID.randomUUID();
			String user_id = uuid.toString().replace( "-", "" ).toLowerCase();
			
			//-- insert
			userDao.addUser(req, user_id);
			
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			if(ErrorUtil.getNestException(e).toLowerCase().indexOf("duplicate entry") >= 0) {
				res.put("code", ApplicationConstant._FAIL_CODE_LOGIN_DUP);
				res.put("message", ApplicationConstant._FAIL_CODE_LOGIN_DUP_MSG);
				res.put("messageTech", "Duplicate entry for user");
			}
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
	
	@GetMapping("/users")
	public HashMap<String, Object> getUserAndOrder(
			@RequestHeader(value = "token_id", required = true) String tokenId,
			@RequestHeader(value = "username", required = true) String userName
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		String visitorId = (StringUtils.isEmpty(userName) ? "N/A" : userName);
		String prefix = visitorId+"|getUserAndOrder|";
		
		log.info(prefix);
		try {
			
			UserInfo user = userDao.validateUserToken(userName, tokenId);
			
			if(user == null) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_TOKEN);
				res.put("message", ApplicationConstant._FAIL_CODE_TOKEN_MSG);
				res.put("messageTech", "invalid token_id");
				
				return res;
			}
			
			List<OrderInfo> listOrder = orderDao.getListOrder(user, userName);
			
			res.put("user", user);
			res.put("listOrder", listOrder);
			
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(ServiceException e) {
			throw e;
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
	
	@DeleteMapping("/users")
	public HashMap<String, Object> deleteUser(
			@RequestHeader(value = "token_id", required = true) String tokenId,
			@RequestHeader(value = "username", required = true) String userName
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		String visitorId = (StringUtils.isEmpty(userName) ? "N/A" : userName);
		String prefix = visitorId+"|deleteUser|";
		
		log.info(prefix);
		try {
			
			UserInfo user = userDao.validateUserToken(userName, tokenId);
			
			if(user == null) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_TOKEN);
				res.put("message", ApplicationConstant._FAIL_CODE_TOKEN_MSG);
				res.put("messageTech", "invalid token_id");
				
				return res;
			}
			
			userDao.deleteUser(userName, user.getUserId());
			
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(ServiceException e) {
			throw e;
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
}
