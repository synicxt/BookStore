package com.test.app.api.exception;

import java.util.ArrayList;
import java.util.List;

public class ServiceParameterValidationException extends Exception{

	private static final long serialVersionUID = 1L;

	public ServiceParameterValidationException() {
		super();
	}
	
	private List<String> messages = new ArrayList<>();
	
	public void appendError( String subjectLabel, String message) {
		if(subjectLabel == null)
			return ;
		
		messages.add(new StringBuilder ("[").append(subjectLabel).append("] ").append(message).toString());
	}
	
	public String getMessage() {
		StringBuilder b= new StringBuilder("Service parameter ไม่ถูกต้อง ");
		
		for ( String msg : messages) {
			b.append(" ").append(msg);
		}
		return b.toString();
		
	}
	
	public List<String> getErrorMessages() {
		return messages;
	}
	
}
