package com.test.app.api.exception;

public class UserSessionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4255874305771016906L;
	private String code;

	public UserSessionException(String prefix, String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public UserSessionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserSessionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserSessionException(String code, String message) {
		super(message);
		this.code = code;
	}
	
	public UserSessionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
