package com.test.app.api.exception;

import com.test.app.api.constant.ApplicationConstant;


public class ServiceException extends Throwable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String status;
	private String code;
	private String message;
	private String messageTech;

	public ServiceException() {
		this.code = ApplicationConstant._FAIL;
	}

	public ServiceException(String message, String code, Throwable cause) {
		super(message, cause);
		this.status = ApplicationConstant._FAIL;
		this.code = code;
		this.message = message;
		this.messageTech = cause.toString();
	}

	public ServiceException(Throwable cause) {
		super(cause);
		this.code = ApplicationConstant._FAIL;
		this.messageTech = cause.toString();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageTech() {
		return messageTech;
	}

	public void setMessageTech(String messageTech) {
		this.messageTech = messageTech;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ServiceException [status=" + status + ", code=" + code + ", message=" + message + ", messageTech="
				+ messageTech + "]";
	}
	
	
}
