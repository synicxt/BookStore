package com.test.app.api.constant;

public class ApplicationConstant {

	public static String _SUCCESS = "SUCCESS";
	public static String _FAIL = "FAIL";
	
	public static String _SUCCESS_CODE = "0";
	public static String _FAIL_CODE_DEFAULT = "500";
	
	public static String _REGX_DATE_FORMAT = "^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$";
	
	public static String _REGX_STRING_LEN_5 = "[0-9a-zA-Z]{6,}";
	
	public static String _FAIL_CODE_VALIDATE = "701";
	public static String _FAIL_CODE_VALIDATE_MSG = "parameter ไม่ถูกต้อง";
	
	public static String _FAIL_CODE_TOKEN = "702";
	public static String _FAIL_CODE_TOKEN_MSG = "login หมดอายุหรือไม่ถูกต้อง กรุณา login ใหม่ และ ทำรายการอีกครั้ง";
	
	public static String _FAIL_CODE_VALIDATE_ID = "703";
	public static String _FAIL_CODE_VALIDATE_ID_MSG = "รหัสหนังสือ  ${param} ไม่ถูกต้อง หรือไม่มีในระบบ กรุณาระบุรหัสหนังสือให้ถูกต้อง";
	
	public static String _FAIL_CODE_STR_LEN = "704";
	public static String _FAIL_CODE_STR_LEN_MSG = "อย่างน้อย 5 ตัวอักษร";
	
	public static String _VALIDATE_NOT_EMPTY = "ต้องไม่เป็นค่าว่าง";
	public static String _VALIDATE_NOT_DATE = "รูปแบบวันที่ไม่ถูกต้อง โปรดระบุ DD/MM/YYYY";
	
	public static String _FAIL_CODE_LOGIN = "801";
	public static String _FAIL_CODE_LOGIN_MSG = "username หรือ password ไม่ถูกต้อง";
	
	public static String _FAIL_CODE_LOGIN_DUP = "802";
	public static String _FAIL_CODE_LOGIN_DUP_MSG = "username นี้มีในระบบแล้ว ไม่สามารถทำการสร้างซ้ำได้";
	
	
	public static String _MSG_INVALID_USER_SESS = "ไม่พบข้อมูลการ login, กรุณาทำการ login และทำรายการใหม่อีกครั้ง ";
	
	public static String _ERROR_EXT_API_BOOK = "ไม่สามารถเชื่่อมต่อ api : Book Publisher Web Service ได้ กรุณารอสักครู่ แล้วทำรายการใหม่อีกครั้ง ";
	
	public static String _MSG_ERROR_DEFAULT = "ขออภัยในความไม่สะดวก ระบบไม่สามารถทำรายการได้ในขณะนี้ กรุณารอสักครู่ และ ลองใหม่อีกครั้ง หรือ ติดต่อ 0xxxxx, test@yyy.com";
}
