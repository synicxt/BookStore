package com.test.app.api.entity;

public class BookInfoOrder extends BookInfo {
	private int quantity;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return super.toString()+", BookInfoOrder [quantity=" + quantity + "]";
	}
	
	
}
