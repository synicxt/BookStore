package com.test.app.api.entity;

import java.util.ArrayList;
import java.util.List;

public class OrderInfo {
	private String orderId;
	private String oderDate;
	private String shippingAddress;
	private double totalPrice = 0.00d;
	
	private List<BookInfoOrder> bookList = new ArrayList<BookInfoOrder>();

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOderDate() {
		return oderDate;
	}

	public void setOderDate(String oderDate) {
		this.oderDate = oderDate;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<BookInfoOrder> getBookList() {
		return bookList;
	}

	public void setBookList(List<BookInfoOrder> bookList) {
		this.bookList = bookList;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString() {
		return "OrderInfo [orderId=" + orderId + ", oderDate=" + oderDate + ", shippingAddress=" + shippingAddress
				+ ", totalPrice=" + totalPrice + ", bookList=" + bookList + "]";
	}

}
