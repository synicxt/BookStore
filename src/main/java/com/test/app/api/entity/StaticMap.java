package com.test.app.api.entity;

import java.util.HashMap;

public class StaticMap {
	private static HashMap<Integer, BookInfo> allMap = new HashMap<Integer, BookInfo>();

	public static HashMap<Integer, BookInfo> getAllMap() {
		return allMap;
	}

	public static void setAllMap(HashMap<Integer, BookInfo> allMap) {
		StaticMap.allMap = allMap;
	} 
	
	
}
