package com.test.app.api.entity;

import java.util.ArrayList;
import java.util.List;

public class OrderCreateInfo {
	private String shippingAddress;
	private List<OrderItem> itemList = new ArrayList<OrderItem>();
	
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public List<OrderItem> getItemList() {
		return itemList;
	}
	public void setItemList(List<OrderItem> itemList) {
		this.itemList = itemList;
	}
	@Override
	public String toString() {
		return "OrderCreateInfo [shippingAddress=" + shippingAddress + ", itemList=" + itemList + "]";
	}
	
	
}
