package com.test.app.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInfo {
	
	@JsonIgnore
	private String userId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	private String name;
	private String surname;
	
	@JsonProperty("date_of_birth")
	private String dateOfBirth;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", name=" + name + ", surname=" + surname + ", dateOfBirth=" + dateOfBirth
				+ "]";
	}
	
	
}
