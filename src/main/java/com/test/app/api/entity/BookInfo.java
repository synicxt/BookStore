package com.test.app.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookInfo {
	private int id;
	
	@JsonProperty("book_name")
	private String name;
	
	@JsonProperty("author_name")
	private String author;
	
	private double price = 0.00d;
	
	@JsonProperty("is_recommended")
	private boolean recommended;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isRecommended() {
		return recommended;
	}

	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	
	
}
