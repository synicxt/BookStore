package com.test.app.api.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.entity.BookInfo;
import com.test.app.api.exception.ServiceException;
import com.test.app.api.utils.ErrorUtil;
import com.test.app.api.utils.ServiceClientUtil;
import com.test.app.api.utils.SortByName;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BookService {
	
	@Value("${book.url.all}")
	private String bookUrlAll;
	
	@Value("${book.url.recommend}")
	private String bookUrlRecommend;
	
	@Value("${book.url.timeout_sec:10}")
	private int bookUrlTimeout;
	
	@Autowired
	private ServiceClientUtil servClientUtil;

	@Cacheable("all_book")
	public List<BookInfo> getAllBooks(String visitorId) throws ServiceException, Throwable{
		String prefix = visitorId+"|getAllBooks|";
		log.info(prefix+"|start");
		
		List<BookInfo> list = new ArrayList<BookInfo>();
		
		try {
			String url = bookUrlAll;
			RestTemplate restTemplate = new RestTemplate(servClientUtil.getClientHttpRequestFactory(url, bookUrlTimeout));
			log.info(prefix+"call url="+url);
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Content-Type", "application/json");
			
			// build the request
		 
		    BookInfo[] resp = restTemplate.getForObject(url, BookInfo[].class);
		    list = Arrays.asList(resp);
		    
		    if(list != null)
		    	Collections.sort(list, new SortByName());
		}
		catch (HttpStatusCodeException e) {
			String errorpayload = ErrorUtil.getNestException(e,500);
			String errorBody = e.getResponseBodyAsString();
			
			log.error(prefix + "HttpStatusCodeException exception=" + errorpayload);
			log.error(prefix + "HttpStatusCodeException errorBody=" + errorBody);

			throw new ServiceException(ApplicationConstant._ERROR_EXT_API_BOOK, ""+e.getRawStatusCode(), e);
		}
		
		return list;
	}
	
	@Cacheable("recommendation_book")
	public List<BookInfo> getRecomendBooks(String visitorId) throws ServiceException, Throwable{
		String prefix = visitorId+"|getRecomendBooks|";
		log.info(prefix+"|start");
		
		List<BookInfo> list = new ArrayList<BookInfo>();
		
		try {
			String url = bookUrlRecommend;
			RestTemplate restTemplate = new RestTemplate(servClientUtil.getClientHttpRequestFactory(url, bookUrlTimeout));
			log.info(prefix+"call url="+url);
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Content-Type", "application/json");
			
			// build the request
		 
		    BookInfo[] resp = restTemplate.getForObject(url, BookInfo[].class);
		    list = Arrays.asList(resp);
		    
		    if(list != null)
		    	Collections.sort(list, new SortByName());
		}
		catch (HttpStatusCodeException e) {
			String errorpayload = ErrorUtil.getNestException(e,500);
			String errorBody = e.getResponseBodyAsString();
			
			log.error(prefix + "HttpStatusCodeException exception=" + errorpayload);
			log.error(prefix + "HttpStatusCodeException errorBody=" + errorBody);

			throw new ServiceException(ApplicationConstant._ERROR_EXT_API_BOOK, ""+e.getRawStatusCode(), e);
		}
		
		return list;
	}
}
