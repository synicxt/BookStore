package com.test.app.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.entity.BookInfo;
import com.test.app.api.entity.StaticMap;
import com.test.app.api.exception.ServiceException;
import com.test.app.api.service.BookService;
import com.test.app.api.utils.ErrorUtil;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CatalogService {
	
	@Value("${spring.application.name:default_value}")
	private String applicatoinName;
	
	@Autowired
	BookService service;
	
	@GetMapping("/books")
	public HashMap<String, Object> getBooks(
			@RequestHeader(value = "token_id", required = false) String tokenId,
			@RequestHeader(value = "username", required = false) String userName
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		String visitorId = (StringUtils.isEmpty(userName) ? "N/A" : userName);
		String prefix = visitorId+"|getBooks|";
		
		log.info(prefix);
		HashMap<Integer, BookInfo> allMap = new HashMap<Integer, BookInfo>();
		try {
			
			List<BookInfo> result = new ArrayList<BookInfo>();
			
			List<BookInfo> r1 = service.getRecomendBooks(visitorId);
			List<BookInfo> r2 = service.getAllBooks(visitorId);
			
			HashMap<Integer, BookInfo> recMap = new HashMap<Integer, BookInfo>();
			for(BookInfo x:r1) {
				recMap.put(x.getId(), x);
				x.setRecommended(true);
				result.add(x);
			}
			
			for(BookInfo y:r2) {
				if(recMap.get(y.getId()) == null) {
					result.add(y);
					allMap.put(y.getId(), y);
				}
			}
			
			allMap.putAll(recMap);
			StaticMap.setAllMap(allMap);
			
			res.put("bookList", result);
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(ServiceException e) {
			throw e;
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
	
}
