package com.test.app.api;

import java.util.HashMap;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.dao.UserDao;
import com.test.app.api.entity.LoginRequest;
import com.test.app.api.utils.ErrorUtil;
import com.test.app.api.utils.ServiceParameterValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class AuthenService {
	
	@Autowired
	UserDao userDao;
	
	@PostMapping("/login")
	public HashMap<String, Object> login(
			@RequestBody LoginRequest req
	) throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		String visitorId = (StringUtils.isEmpty(req.getUsername()) ? "N/A" : req.getUsername());
		String prefix = visitorId+"|login|";
		log.info(prefix);
		
		ServiceParameterValidator.getInstance()
		.setSubject("username", req.getUsername())
		.required(ApplicationConstant._VALIDATE_NOT_EMPTY)
		
		.setSubject("password", req.getPassword())
		.required(ApplicationConstant._VALIDATE_NOT_EMPTY)
		.validate();
		
		try {
			
			UUID uuid = UUID.randomUUID();
			String token_id = uuid.toString().replace( "-", "" ).toLowerCase();
			
			String user_id = userDao.login(req);
			
			if(StringUtils.isEmpty(user_id)) {
				res.put("status", ApplicationConstant._FAIL);
				res.put("code", ApplicationConstant._FAIL_CODE_LOGIN);
				res.put("message", ApplicationConstant._FAIL_CODE_LOGIN_MSG);
				res.put("messageTech", "login failed");
				
				return res;
			}
			
			//-- update token_id to user
			userDao.updateToken(req.getUsername(), token_id);
			
			res.put("token_id", token_id);
			res.put("status", ApplicationConstant._SUCCESS);
			res.put("code", ApplicationConstant._SUCCESS_CODE);
		}
		catch(Exception e) {
			res.put("status", ApplicationConstant._FAIL);
			res.put("code", ApplicationConstant._FAIL_CODE_DEFAULT);
			res.put("message", ApplicationConstant._MSG_ERROR_DEFAULT);
			res.put("messageTech", ErrorUtil.getNestException(e));
			
			log.error(prefix+"ERR "+ErrorUtil.getNestException(e));
		
			return res;
		}
		
		return res;
	}
}
