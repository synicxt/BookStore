package com.test.app.api.utils;

import java.util.Arrays;

public class ErrorUtil {
	public static String getNestException(Throwable t, String lookfor) {
		String err = t.toString();
		
		if(lookfor != null) {
			if(err.lastIndexOf(lookfor) >= 0) {
				err = err.substring(err.lastIndexOf(lookfor), err.length());
			}
		}
		
		return err;
	}
	
	public static String getNestException(Throwable t, int len) {
		String err = t+" : "+Arrays.toString(t.getStackTrace());
		
		if(err.length() > len) err = err.substring(0, len);
		
		return err;
	}
	
	public static String getNestException(Throwable t) {
		return getNestException(t, 500);
	}
}
