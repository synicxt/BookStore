package com.test.app.api.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.test.app.api.exception.ServiceParameterValidationException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceParameterValidator {

	private String label;
	private Object value;
	
	private ServiceParameterValidationException e;
	
	private ServiceParameterValidator() {
		
	}
	
	public static ServiceParameterValidator getInstance() {
		return new ServiceParameterValidator();
	}
	
	/**
	 * Set subject to be validate
	 * @param label
	 * @param value
	 */
	public ServiceParameterValidator setSubject(String label, Object value) {
		this.label = label;
		this.value= value;
		return this;
	}
	
	/**
	 * Validate required
	 * @param message
	 * @return
	 */
	public ServiceParameterValidator required(String message) {
		log.debug("Validating require on subject: {} value: {}", label, value);
		if(StringUtils.isEmpty(value))
			addValidationMessage(label, message==null?"required":message);
		
		return this;
	}
	
	/**
	 * Validate date format
	 * @param message
	 * @param dateFormat
	 * @return
	 */
	public ServiceParameterValidator isDate(String message, String dateFormat) {
		
		if(value instanceof Date)
			return this;
		if(value == null) {
			addValidationMessage(label, message==null?"Invalid date format, object is null":message);
			return this;
		}
		
		DateFormat df = null;
		try { 
			df = new SimpleDateFormat("yyyy-MM-dd");
			Date d = df.parse(value.toString());
			log.info("isDate "+d);
			return this;
		} catch(ParseException e) {
			addValidationMessage(label, message==null?"Invalid date format":message);
			return this;
		}catch (Throwable t) {
			log.error(e.getMessage(), e);
			addValidationMessage(label, "Error occur in validation process");
			return this;
		}
	}
	
	
	/**
	 * Validate value matched with regular expression
	 * @param message
	 * @param regexp
	 * @return
	 */
	public ServiceParameterValidator matched(String message, String regex) {
		log.debug("Validating matched on subject: {} value: {} with regex:{}", label, value, regex);
		
		if( 
				value == null 
				||
				(value != null && value instanceof String && regex != null && !Pattern.matches(regex, value.toString()))
			) {
			addValidationMessage(label, message==null?"not matched with regular pattern [" + regex+"]":message);
		}
		
		return this;
	}
	
	public void validate() throws ServiceParameterValidationException{
		if(e != null)
			throw e;
	}
	
	private void addValidationMessage(String subjectLabel, String message) {
		if(subjectLabel == null  || message == null)
			return ;
		
		e = e==null?new ServiceParameterValidationException():e;
		e.appendError(subjectLabel, message);
		
	}
	
}
