package com.test.app.api.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class ServiceClientUtil {

	public SimpleClientHttpRequestFactory getClientHttpRequestFactory(String url, int timeout_sec) 
	{
	    SimpleClientHttpRequestFactory clientHttpRequestFactory
	                      = new SimpleClientHttpRequestFactory();
	    
	    int con_timeout = 10;
	    int read_timeout = 10;
	    
	    if(timeout_sec > 0) {
	    	con_timeout = timeout_sec;
	    	read_timeout = timeout_sec;
	    }

	    log.info("SimpleClientHttpRequestFactory URL="+url+" timeout="+con_timeout);
	    //Connect timeout
	    clientHttpRequestFactory.setConnectTimeout(con_timeout * 1000);
	     
	    //Read timeout
	    clientHttpRequestFactory.setReadTimeout(read_timeout * 1000);
	    return clientHttpRequestFactory;
	}
	
	public HttpHeaders genRequestHeaders(HashMap<String, String> map) {
		HttpHeaders header = new HttpHeaders();
		if(map != null) {
			for (Map.Entry mapElement : map.entrySet()) { 
	            String key = (String) mapElement.getKey(); 
	            String value = (String) mapElement.getValue(); 
	            
	            header.set(key , value);
			}
		}
		
		return header;
	}
}
