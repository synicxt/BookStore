package com.test.app.api.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {

	public static String getMD5(String data) throws NoSuchAlgorithmException
    { 
		MessageDigest messageDigest=MessageDigest.getInstance("MD5");

        messageDigest.update(data.getBytes(), 0, data.length());
       //  byte[] digest=messageDigest.digest();
        
        String hashedPass = new BigInteger(1,messageDigest.digest()).toString(16);  
        if (hashedPass.length() < 32) {
        	   hashedPass = "0" + hashedPass; 
        }
        
        return hashedPass;
    }
}
