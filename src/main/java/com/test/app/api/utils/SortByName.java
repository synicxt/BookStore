package com.test.app.api.utils;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

import com.test.app.api.entity.BookInfo;

public class SortByName implements Comparator<BookInfo>  {

	Collator collator = Collator.getInstance(Locale.US);
	
	@Override
	public int compare(BookInfo o1, BookInfo o2) {
		// TODO Auto-generated method stub
		return collator.compare(o1.getName(), o2.getName());
	}

}
