package com.test.app;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.test.app.api.constant.ApplicationConstant;
import com.test.app.api.exception.ServiceException;
import com.test.app.api.exception.ServiceParameterValidationException;
import com.test.app.api.exception.UserSessionException;
import com.test.app.api.utils.ErrorUtil;


@ControllerAdvice(annotations = RestController.class)
public class CustomServierExceptionHandler {

	@ExceptionHandler({ UserSessionException.class})
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<Object> handleBadRequests(HttpServletResponse response, UserSessionException e)
			throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		res.put("status", "FAIL");
		res.put("code", "401");
		res.put("message", ApplicationConstant._MSG_INVALID_USER_SESS);
		res.put("messageTech", ErrorUtil.getNestException(e));
	
		return new ResponseEntity<Object>(res, HttpStatus.UNAUTHORIZED);
	
	}
	
	@ExceptionHandler({ ServiceException.class})
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<Object> handleBadRequests(HttpServletResponse response, ServiceException e)
			throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		res.put("status", "FAIL");
		res.put("code", e.getCode());
		res.put("message", e.getMessage());
		res.put("messageTech", e.getMessageTech());
	
		return new ResponseEntity<Object>(res, HttpStatus.OK);
	
	}
	
	@ExceptionHandler({ ServiceParameterValidationException.class})
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<Object> handleBadRequests(HttpServletResponse response, ServiceParameterValidationException e)
			throws Throwable {
		HashMap<String, Object> res = new HashMap<String, Object>();
		res.put("status", "FAIL");
		res.put("code", ApplicationConstant._FAIL_CODE_VALIDATE);
		res.put("message", e.getMessage());
		res.put("messageTech", "invalid parameter");
	
		return new ResponseEntity<Object>(res, HttpStatus.OK);
	
	}
	
}
