package com.test.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    
    @Autowired
    Environment env;
    
    @Bean
    public Docket selectApi() {
	Docket docs = new Docket(DocumentationType.SWAGGER_2)
	    .select()
	    .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
	    .paths(PathSelectors.any())
	    .build()
	    .useDefaultResponseMessages(false)
	    .apiInfo(this.apiInfo());
	return docs;

    }

    @SuppressWarnings("unused")
    private Docket setGlobalParams(Docket docket) {
	List<Parameter> globalParams = new ArrayList<Parameter>();
	
	docket.globalOperationParameters(globalParams);
	return docket;
    }

    private ApiInfo apiInfo() {
	// return ApiInfo.DEFAULT;
	return new ApiInfo("BookStore Restful webservice",
			   "BookStore REST",
			   "0.1",
			   "http://localhost:8888",
			   new Contact("Jedsada phonkla", ".", "synicxt@gmail.com"),
			   "License of API",
			   "API license URL",
			   Collections.emptyList());
    }

}
